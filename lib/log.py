from datetime import datetime

def now():
    return datetime.now().replace(microsecond=0).isoformat(" ")

def log(message: str):
    print(f"[{now()}]: {message}")
