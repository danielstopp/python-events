from typing import Callable


subscribers = dict()

def subscribe(event: str, fn: Callable):
    if not event in subscribers:
        subscribers[event] = []
    subscribers[event].append(fn)

def post_event(event: str, data: any):
    if not event in subscribers:
        return
    for fn in subscribers[event]:
        fn(data)
