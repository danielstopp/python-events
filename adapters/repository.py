from typing import List
from abc import abstractmethod, ABC

from domain import model


UserList = List[model.User]

class AbstractUserRepository(ABC):

    @abstractmethod
    def add(self, user: model.User) -> None:
        raise NotImplementedError

    @abstractmethod
    def get(self, email: str) -> model.User:
        raise NotImplementedError

    @abstractmethod
    def remove(self, email: str) -> model.User:
        raise NotImplementedError

    @abstractmethod
    def list(self) -> UserList:
        raise NotImplementedError


class FakeUserRepository(AbstractUserRepository):

    def __init__(self, users: UserList) -> None:
        self._users = set(users)

    def add(self, user: model.User) -> None:
        self._users.add(user)

    def get(self, email: str) -> model.User:
        return next((user for user in self._users if user.email == email), None)

    def remove(self, email: str) -> None:
        user = self.get(email)
        self._users.remove(user)

    def list(self) -> UserList:
        return list(self._users)
