from adapters.repository import FakeUserRepository
from adapters.event import post_event
from domain import model


user_repo = FakeUserRepository([])

def register(username: str, email: str, password: str):
    if user_repo.get(email) is None:
        user = model.User(username, email, password)
        user_repo.add(user)
        post_event("user_registered", user)
        return user

    return {"error": f"User with email {email} already exists", "code": 400}

def suspend_user(email: str):
    user = user_repo.get(email)
    if user is not None:
        user_repo.remove(email)
        post_event("user_banned", user)
        return user

    return {"error": f"User with email {email} not found", "code": 404}

