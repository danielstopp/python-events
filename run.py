from time import sleep

from api.user import register, suspend_user
from listeners.log import setup_log_event_handlers
from listeners.email import setup_email_event_handlers


setup_log_event_handlers()
setup_email_event_handlers()

register("someguy", "some@guy.de", "test")
sleep(1)
suspend_user("some@guy.de")
