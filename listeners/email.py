from lib.email import send_email
from adapters.event import subscribe


def handle_user_registered_event(user):
    send_email(
        user.username,
        user.email,
        "Your registration",
        f"Welcome, {user.username}!"
    )

def handle_user_banned_event(user):
    send_email(
        user.username,
        user.email, 
        "Ts ts ts ...",
        f"Your account has been permanently suspended for being an annoying prick."
    )

def setup_email_event_handlers():
    subscribe("user_registered", handle_user_registered_event)
    subscribe("user_banned", handle_user_banned_event)

