from lib.log import log
from adapters.event import subscribe


def handle_user_registered_event(user):
    log(f"User with email `{user.email}` registered")

def handle_user_banned_event(user):
    log(f"User with email `{user.email}` banned")

def setup_log_event_handlers():
    subscribe("user_registered", handle_user_registered_event)
    subscribe("user_banned", handle_user_banned_event)
